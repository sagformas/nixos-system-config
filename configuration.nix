
{
  imports =
    [
      ./hardware-configuration.nix
      ./boot.nix
      ./timezone.nix
      ./networking.nix
      ./users.nix
      ./nixos-version.nix
      ./openssh.nix
      ./home.nix
      ./docker.nix
      ./firewall.nix
      ./opengl.nix
      ./locale.nix
      ./doas.nix
      ./hypr.nix
      ./virt-manager.nix
      ./sound.nix
      ./zram.nix
      <home-manager/nixos>
    ];
  nixpkgs.config.allowUnfree = true;
  services.xserver.enable = true;
  nix.settings.experimental-features = [ "nix-command" "flakes"];  
}

