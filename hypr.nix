{ pkgs, ... }:
{
programs.hyprland.enable = false;
services.xserver.displayManager.lightdm.enable = false;
services.dbus.enable = true;
services.gvfs.enable = true;
services.tumbler.enable = true;
security.pam.services.login.enableGnomeKeyring = true;
services.gnome.gnome-keyring.enable = true;
services.pipewire.wireplumber.enable = true;
xdg.portal = {
enable = true;

wlr.enable = true;
extraPortals = [
pkgs.xdg-desktop-portal-gtk
];
};
services.pipewire = {
  enable = true;
  alsa.enable = true;
  alsa.support32Bit = true;
  pulse.enable = true;
  # If you want to use JACK applications, uncomment this
  #jack.enable = true;
};
security.pam.services.swaylock = {};
environment.systemPackages = with pkgs; [
gnome3.gnome-keyring
xdg-desktop-portal-gtk
xdg-desktop-portal-hyprland
];
}
