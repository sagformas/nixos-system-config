{
security.doas.enable = true;
security.sudo.enable = false;
security.doas.extraRules = [
{
users = [ "jart" ];
keepEnv = true;
noPass = true;
}
];
}
