{ pkgs, ... }:
{
users.users.jart = {
     isNormalUser = true;
     extraGroups = [ "wheel" "video" "networkmanager" "audio" "docker" "libvirtd" ];
   };
programs.zsh.enable = true;
users.users.jart.shell = pkgs.zsh;
}
